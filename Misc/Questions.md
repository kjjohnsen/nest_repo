#QUESTIONS TO ASK DR. LAWRENCE & DR. JOHNSEN#

1.  What data should be available to building manager?
    * Should the building manager be able to see room-by-room data? Or should it be a general overview of each building?
    * Should they be able to see all the data that we have collected, or just some (we need to figure out what data we will be logging)
    * Don't want to be able see away times?
2.  What control should the end-user have?
3.  How often should the data be refreshed?
    * Every 1 second? Every 30 mins?
    * User should be able to manually refresh their account
4.  What should be the name of the website?


    
*********************************************************************************************

#QUESTIONS TO ASK OUR OWN GROUP#

1. How secure do we want our page to be?
   *Session storing or something more sophisticated?