Prohibitions list

Implement functionality that interferes with the core functionality of Nest products or services.

Collect, aggregate, re-syndicate, retain, log or store Customer Data (as defined below) received via the Nest API beyond 10 trailing days from the date when the data is received.

Aggregate control of Nest products, services, or Customer Data across multiple households except to the extent Nest permits control of multiple homes in a single Nest account.

Create a Client that performs demand response or other energy management programs such as those offered by electric, gas, water or similar companies or energy markets.

Offer or advertise a Client that provides emergency response, notification services, life-safety, or other critical use services that require notifications to be provided without interruption.

Create a Client or otherwise use Customer Data to evaluate end users or their property individually or in aggregate for insurance or other financial products and services.

Harm, defame, abuse, harass, stalk, threaten, endanger the safety, or violate the legal rights (such as rights of privacy and publicity) of any person or encourage any third party to do the same.

Upload, post, transmit or otherwise make available any inappropriate, defamatory, obscene, or unlawful content.

Create a Client that functions substantially the same as the Nest API or other Nest Developer Materials and offer it for use by third parties.

Perform an action with the intent of introducing to Nest products and services any viruses, worms, defects, Trojan horses, malware or any items of a destructive nature.

Access or control any customer accounts or any devices linked to any customer accounts in a manner that could cause any harm, damage, or loss.

Use the Nest API to process or store any data that is subject to the International Traffic in Arms Regulations maintained by the Department of State.