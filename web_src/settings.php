<?php
session_start();
/*ini_set("display_errors","on");
  error_reporting(E_ALL);
  */
error_reporting(0);

require_once('setup.php');

//PREFERENCES / SETTINGS
checkLogin($_SESSION['isValid']);
$updated = false;

if(isset($_POST['opt_in'])){
  connect();
  //global $mysqli;
  
  $query = "UPDATE nest_room_number_X_account SET opt_in =".$_POST['opt_in']." WHERE thermostat_account_id=".$_SESSION['thermostatAccountID'];
  $results =$mysqli->query($query) or die(mysqli_error($mysqli));
  if($results){
    $_SESSION['opt_in'] = $_POST['opt_in'];
    $updated= true;
  }
  
}

?>

<html>
  <head>

   <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
      <meta name="author" content="">
  
   <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/navbar.css" rel="stylesheet">
  </head>
  <body>
    <script>
    </script>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home.php">Nest Apartment Manager</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="home.php">Home</a></li>
              <li><a href="usage.php">Usage</a></li>
       	      <li><a href="./logs.php">Logs</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><p class="navbar-text"><?php echo $_SESSION['MyID']; ?></p></li>
              <li class="active"><a href=''>Settings</a></li>
              <li><a href="./logout.php">Logout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div><!--end container-->

    <div class="container">
      <div class="row row-centered">
        <div class="col-xs-12 col-sm-8  col-centered">
          <div class="jumbotron">
            <div id="SelectBox">     
              <div id="txtHint"></div>
              <div class="centered">
                <?php
                //echo "<h1>This needs to be fixed so it looks good </h1>";
                if($_SESSION['opt_in']){
                  echo "<h2>You are currently allowing your building manager to control your thermostat.<br><br></h2>";              
                }
                else{
                  echo "<h2>You are <span style='color:red'>not</span> currently allowing your building manager to control your thermostat.<br><br></h2>";
                }

                echo "<form action='' method='post'>";
                echo "<p>Allow my building manager to remotely control my Thermostat:</p>";
                echo "<p><input type='radio' name='opt_in' value='1'>Yes  <input type='radio' name='opt_in' value='0'>No</p>";
                echo "<input type='submit' value='Update!'></form>";
                if($updated){
                    echo"<h3>Your preferences have been updated.</h3>";
                }

                ?>
              </div><!--end of centered--> 
            </div><!--end of selectbox--> 
          </div><!--end of jumbotron--> 
        </div><!-- end of column-->
      </div><!-- end of row-->
    </div> <!--end container-->
  </body>
</html>
