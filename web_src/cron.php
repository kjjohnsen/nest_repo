<?php  	
  	
	/*ini_set("display_errors","on");
 	error_reporting(E_ALL);
 	*/
	error_reporting(0);


 	date_default_timezone_set('America/Montreal');
 	



 	require_once('setup.php');
 	



 	require_once('nest.class.php');





?>

<?php

	

	connect();
	

	run();

	

	function run(){
		global $mysqli;
			
		$table = "nest_thermostat_account";
		
		$query = "SELECT * FROM $table";
    	
    	$timestamp = time();
    	echo "current time is: ".$timestamp;
    	$logs = array();
		$results =$mysqli->query($query) or die(mysqli_error($mysqli));
    	

    	while($row = mysqli_fetch_assoc($results)){
 			$username = $row['username'];
 			$password = $row['password'];
 			$nest = new Nest($username, $password);
			$locations = $nest->getUserLocations();
			$devices_serials = $nest->getDevices();
			$infos = $nest->getDeviceInfo($devices_serials[0]);


			$logs[] = array(
			"nest_account_username" => $username,
			"is_heat" => ($infos->current_state->mode=="heat") ? 'TRUE' : 'FALSE',
			"is_away" => ($locations[0]->away==1) ? 'TRUE' : 'FALSE',
			"fan_is_on" => ($infos->current_state->fan==1) ? 'TRUE' : 'FALSE',
			"is_leaf" => ($infos->current_state->leaf==1) ? 'TRUE' : 'FALSE',
			"current_temp" => $infos->current_state->temperature,
			"outside_temp" => $locations[0]->outside_temperature,
			"humidity" => $infos->current_state->humidity,
			"outside_humidity" => $locations[0]->outside_humidity,
			"alt_heat" => ($infos->current_state->alt_heat==1) ? 'TRUE' : 'FALSE',
			"away_last_changed" => $locations[0]->away_last_changed,
			"target_mode" => $infos->target->mode,
			"target_temp" => $infos->target->temperature,
			"time_to_target" => $infos->target->time_to_target
			);
 		}//FOREACH 
		

		storeData($timestamp,$logs);

	}//RUN()


	function storeData($timestamp,$logs){
		global $mysqli;
		$table = "nest_datalog";

		for($i=0;$i<sizeof($logs);$i++){
			$nest_account_username = $logs[$i]['nest_account_username'];
			$is_heat = $logs[$i]['is_heat'];
			$is_away = $logs[$i]['is_away'];
			$fan_is_on = $logs[$i]['fan_is_on'];
			$is_leaf = $logs[$i]['is_leaf'];
			$current_temp = $logs[$i]['current_temp'];
			$outside_temp = $logs[$i]['outside_temp'];
			$humidity = $logs[$i]['humidity'];
			$outside_humidity = $logs[$i]['outside_humidity'];
			$alt_heat = $logs[$i]['alt_heat'];
			$away_last_changed = $logs[$i]['away_last_changed'];
			$target_mode = $logs[$i]['target_mode'];
			$target_temp = $logs[$i]['target_temp'];
			$time_to_target = $logs[$i]['time_to_target'];

			//handle heating/cooling/off/range target temp modes
			if(is_array($target_temp)){
				//echo "YUP!!!";
				$tt_0 = $target_temp[0];
				$tt_1 = $target_temp[1];
				$query = "INSERT INTO 
				$table (nest_account_username,timestamp,is_away,	is_heat,	is_leaf,	fan_is_on,	current_temp,	outside_temp, humidity,outside_humidity, alt_heat,away_last_changed,target_mode,target_temp_low,target_temp_high,time_to_target)
				VALUES('$nest_account_username',$timestamp,'$is_away','$is_heat','$is_leaf','$fan_is_on',$current_temp,	$outside_temp,$humidity,$outside_humidity,'$alt_heat','$away_last_changed','$target_mode',$tt_0,$tt_1,$time_to_target)";
			}else{
				//echo "NOPE....";
				$tt_field = 'target_temp_low';
				if($target_mode=='heat'){
					$tt_field = 'target_temp_high';
				}
				$query = "INSERT INTO 
				$table (nest_account_username,timestamp,is_away,	is_heat,	is_leaf,	fan_is_on,	current_temp,	outside_temp, humidity,outside_humidity, alt_heat,away_last_changed,target_mode,$tt_field,time_to_target)
				VALUES('$nest_account_username',$timestamp,'$is_away','$is_heat','$is_leaf','$fan_is_on',$current_temp,	$outside_temp,$humidity,$outside_humidity,'$alt_heat','$away_last_changed','$target_mode',$target_temp,$time_to_target)";

			}
			
			/*
			echo '<pre>';
			var_dump($query);
			echo '</pre>';
			*/

			$results =$mysqli->query($query) or die(mysqli_error($mysqli));
		}//FOR(I=0...)
		
	}//STOREDATA($TIMESTAMP,$LOGS)
?>
