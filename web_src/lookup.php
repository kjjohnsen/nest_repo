<?php
/*DO NOT ADD HTML COMMENTS TO THIS PAGE OR IT WON'T FUNCTION PROPERLY*/
session_start();
/*ini_set("display_errors","on");
 	error_reporting(E_ALL);
 	*/
	error_reporting(0);

require_once('setup.php');
checkLogin($_SESSION['isValid']);
date_default_timezone_set('America/Montreal'); 
require_once('nest.class.php');
connect();


//echo "hello";
$id = $_GET['q'];
$credentials = array();
$credentials = getCredentials($id);
try{
	$settings = getNestStatus($credentials[0], $credentials[1]);
	$var = json_encode($settings);
	echo $var;
}catch (Exception $e){
	$var = array();
	$var[0] = "NO_NEST";
	echo json_encode($var);
	exit;
}



function getNestStatus($username, $password){
	global $debug;
	$nest = new Nest($username, $password);
	$returnMe = array();
	
	
	$locations = $nest->getUserLocations();
	$devices_serials = $nest->getDevices();	
	


	if(empty($devices_serials)){
		$returnMe[0] = null;
		$returnMe[1] = null;
		$returnMe[2] = null;
		$returnMe[3] = null;
	}
	else{
		$infos = $nest->getDeviceInfo($devices_serials[0]);	
		$returnMe[0] = getTemp($infos);
		$returnMe[1] = getMode($infos);
		$returnMe[2] = getFanMode($infos);
		$returnMe[3] = getAwayMode($locations[0]);
	}
	
	if($debug){print_r($returnMe);}
	return $returnMe;
	
}

function getTemp($infos){

	$var = isset($infos->current_state->mode) ? round($infos->current_state->temperature) : null;
	return $var;

}
function getMode($infos){
	

	$var = isset($infos->current_state->mode) ? $infos->current_state->mode : null;
	return $var;
}
function getFanMode($infos){
	$var = isset($infos->current_state->mode) ? $infos->current_state->fan : null;
	return $var;
}
function getAwayMode($checkMe){
	$var = isset($checkMe->away) ? $checkMe->away : null;
	return $var;
}


?>