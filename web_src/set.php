<?php
/*DO NOT ADD HTML COMMENTS TO THIS PAGE OR IT WON'T FUNCTION PROPERLY*/
session_start();
/*ini_set("display_errors","on");
 	error_reporting(E_ALL);
 	*/
error_reporting(0);

require_once('setup.php');
checkLogin($_SESSION['isValid']);
require_once('nest.class.php'); 


$id = $_GET['therm'];
$credentials = getCredentials($id);

if(strlen($credentials[0]) == 0 || (strlen($credentials[1]) == 0) ){
	$status = json_encode(false);
	exit;
}
$username =  $credentials[0];
$password =  $credentials[1];

try{
	$nest = new Nest($username, $password);
}catch (Exception $e){
	$status = json_encode(false);
	exit;
}



$locations = $nest->getUserLocations();
$devices_serials = $nest->getDevices();

if(empty($devices_serials)){
	$status = json_encode(false);
	exit;
}

$infos = $nest->getDeviceInfo($devices_serials[0]);


if($_GET != null){ //home.php is setting data

	$temperature = $_GET['temp'] == "" ? 69 : $_GET['temp'];
	$fan = $_GET['fan'];
	$away = $_GET['away'];
	$mode = $_GET['mode'];

	if($debug){
		echo 'temperature: ' . $temperature . "<br>";
		echo 'fan: ' . $fan . "<br>";
		echo 'away: ' . $away . "<br>";
		echo 'mode: ' . $mode . "<br>";

	}

	if($debug){
		echo "Setting target temperature to " . $temperature . "<br>";
		if($mode != 'null'){
			echo "Setting mode to " . $mode . "<br>";
		}
	}
	if($mode == "cool"){
		$success = $nest->setTargetTemperatureMode(TARGET_TEMP_MODE_COOL, $temperature);
	}
	else if($mode == "heat"){
		$success = $nest->setTargetTemperatureMode(TARGET_TEMP_MODE_HEAT, $temperature);
	}
	else{
		$success = $nest->setTargetTemperature($temperature);
	}
	if($debug){
		var_dump($success);
		echo "<br>";
	}



	if($fan != "null"){
		if($debug){
			echo "Setting fan mode...\n";
		}
		if($fan=="on"){

			if($debug){
				echo "now turning fan on<br>";
			}
			$success = $nest->setFanMode(FAN_MODE_ON); // Available: FAN_MODE_AUTO or FAN_MODE_EVERY_DAY_OFF, FAN_MODE_ON or FAN_MODE_EVERY_DAY_ON
	// setFanMode() can also take an array as it's argument. See the comments below for examples (FAN_MODE_TIMER, FAN_MODE_MINUTES_PER_HOUR).
		}else if($fan=="off"){
			if($debug){
				echo "now turning fan off<br>";
			}
			$success = $nest->setFanMode(FAN_MODE_EVERY_DAY_OFF); // Available: FAN_MODE_AUTO or FAN_MODE_EVERY_DAY_OFF, FAN_MODE_ON or FAN_MODE_EVERY_DAY_ON
		// setFanMode() can also take an array as it's argument. See the comments below for examples (FAN_MODE_TIMER, FAN_MODE_MINUTES_PER_HOUR).
		}else{
			if($debug){
				echo "now turning fan to auto<br>";
			}
			$success = $nest->setFanMode(FAN_MODE_AUTO); // Available: FAN_MODE_AUTO or FAN_MODE_EVERY_DAY_OFF, FAN_MODE_ON or FAN_MODE_EVERY_DAY_ON
			// setFanMode() can also take an array as it's argument. See the comments below for examples (FAN_MODE_TIMER, FAN_MODE_MINUTES_PER_HOUR).
		}
		if($debug){
			var_dump($success);
			echo "<br>";
		}
	}


	if($away !="null") {
		
		if($debug){
			echo "Setting away mode...\n";
		}
		
		if($away == 'on'){
			if($debug){
				echo "now turning on away mode";
			}
			$success = $nest->setAway(AWAY_MODE_ON); // Available: AWAY_MODE_ON, AWAY_MODE_OFF
		}
		else if($away == 'off'){ 
			if($debug){
				echo "now turning off away mode";
			}
			$success = $nest->setAway(AWAY_MODE_OFF);
		}
		else{
			$success = $nest->setAutoAwayEnabled(true);	
		}
		if($debug){
			var_dump($success);
			echo "<br>";
		}
	}
}


$status = json_encode(true);
?>
