<?php
  session_start();
/*ini_set("display_errors","on");
  error_reporting(E_ALL);
  */
  error_reporting(0);

?>
<!--GRAPHS/VISUALIZATION STUFF-->
<?php
  require_once('setup.php');
  checkLogin($_SESSION['isValid']);
?>

<html>
  <head>

   <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
      <meta name="author" content="">
  <script>
    function SetMe(){
  //        alert('THIS RAN') 
           if (false) {
         // document.getElementById("txtHint").innerHTML = "";
    
          return;
      } else {
          if (window.XMLHttpRequest) {
              // code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp = new XMLHttpRequest();
          } else {
              // code for IE6, IE5
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange = function() {
              if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                  document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
              }
          }

          
    
          xmlhttp.open("GET","stats.php?");
          xmlhttp.send();
      }
    

  }
  </script>
   <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/navbar.css" rel="stylesheet">
  </head>
  <body onload="SetMe()">
    <script>
    </script>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home.php">Nest Apartment Manager</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="home.php">Home</a></li>
              <li><a href="usage.php">Usage</a></li>
       	      <li class="active"><a href="#">Logs</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><p class="navbar-text"><?php echo $_SESSION['MyID']; ?></p></li>
              <li><a href='./settings.php'>Settings</a></li>
              <li><a href="./logout.php">Logout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div><!--end container-->

    <div class="container">
      <div class="row row-centered">
        <div class="col-xs-12 col-sm-12  col-centered">
          <div class="jumbotron">    
              <div id="txtHint"></div>
              <div class="centered">
                <div id="types" class="btn-group">
                  <center><button name="color" type="button" class="btn btn-success"  value="btn-success"  onclick="SetMe()">Refresh</button></center>	
                </div>
              </div><!--end of centered--> 
          </div><!--end of jumbotron--> 
        </div><!-- end of column-->
      </div><!-- end of row-->
    </div> <!--end container-->
  </body>
</html>
