<?php
	session_start();
/*ini_set("display_errors","on");
 	error_reporting(E_ALL);
 	*/
	error_reporting(0);

?>

<?
	require_once('setup.php');
	checkLogin($_SESSION['isValid']);


	if($debug){
		echo "now running in debug mode<br>"; 
	}

	if($debug){
		print_r($_SESSION);
		echo "<br>";
 	}

	date_default_timezone_set('America/Montreal');    

	require_once('nest.class.php');


	$username =  $_SESSION['thermostatUsername'];
	$password =  $_SESSION['thermostatPassword'];
 
?>


<?php

	$nest = new Nest($username, $password);

	$locations = $nest->getUserLocations();

	$devices_serials = $nest->getDevices();

	$infos = $nest->getDeviceInfo($devices_serials[0]);

	printTable($infos,$locations);

	printOldLogs($infos,$locations);
?>

<?
	
	function printOldLogs($infos,$locations){
	}

	function printTable($infos,$locations){
		echo "<pre>";
		echo('<table class="table table-bordered">');
		echo ('<tr><td>Thermostat Name</td><td>').$locations[0]->name .('</td></tr>');
		//echo ('<tr><td>Target Mode</td><td>').$infos->target->mode .('</td></tr>');
		echo ('<tr><td>Current Mode</td><td>').$infos->current_state->mode .('</td></tr>');
		//echo ('<tr><td>Target Temperature</td><td>').$infos->target->temperature .('</td></tr>');
		echo ('<tr><td>Temperature</td><td>').$infos->current_state->temperature .('</td></tr>');
		echo ('<tr><td>Humidity</td><td>').$infos->current_state->humidity .('</td></tr>');
		echo '<tr><td>Outside Temperature</td><td>'. $locations[0]->outside_temperature.'</td></tr>';

		echo'<tr><td>Outside Humidity</td><td>'. $locations[0]->outside_humidity.'</td></tr>';

		echo'<tr><td> Away Mode</td><td>'. checkisset($locations[0]->away).'</td></tr>';

		echo'<tr><td>Away Last changed</td><td>'. $locations[0]->away_last_changed.'</td></tr>';
		echo ('<tr><td>Fan</td><td>').checkisset($infos->current_state->fan) .('</td></tr>');
		echo ('<tr><td>Leaf</td><td>').booleanToString($infos->current_state->leaf) .('</td></tr>');
		echo ('<tr><td>Battery Level</td><td>').$infos->current_state->battery_level .('</td></tr>');

		echo'</table>';
	}


	function checkisset($str){

		$str = strlen($str) != 0? $str : 'false';
		$str = $str =="1" ? "true" : $str;
		$str = $str =="0" ? "false": $str;

		return $str;
	}

	function booleanToString($str){

		if($str =="1"){
			return 'true';
		}
		return 'false';

	}

	function getTemp(){
		return $infos->current_state->temperature;
	}


?>
