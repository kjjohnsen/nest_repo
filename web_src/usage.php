<?php
	session_start();
	
	ini_set("display_errors","on");
 	error_reporting(E_ALL);

	require_once('setup.php');
	checkLogin($_SESSION['isValid']);
	
	$roomArray = array();

	$myID = $_SESSION['MyID'];
	$adminExtra = "";
	if($_SESSION['isAdmin']){
                      
		$temp = getRooms();  
		$adminExtra = "<label for='roomStuff'>Select Rooms to Compare</label>
				<div id='roomStuff' name='roomStuff'>
		    <select class='btn btn-default' id='roomChange' name='roomChange'>
		    <option disabled value='disabled' hidden>Select First Room</option>";

		//$roomArray = array();
		for($i = 0; $i<sizeof($temp); $i++){
		  
			$result = str_replace('_',' ',$temp[$i][2]);
			$roomArray[$temp[$i][4]] = $result.' '.$temp[$i][3];
			$adminExtra = $adminExtra."<option value=".$temp[$i][4];

			if($i==0){
			  $adminExtra = $adminExtra.' selected';
			}

			$adminExtra = $adminExtra.'>'.$result.' '.$temp[$i][3].'</option>';

		}//end for i=0->sizeof(temp)

		$adminExtra = $adminExtra."</select><br><select class='btn btn-default' id='roomChange1' name='roomChange1'>
		  							<option disabled value='disabled' selected hidden>Select Second Room</option>
		  								<option value='disabled'>(None)</option>";
		
		for($i = 0; $i<sizeof($temp); $i++){
			$result = str_replace('_',' ',$temp[$i][2]);
			$adminExtra = $adminExtra."<option value=".$temp[$i][4]."> $result ".$temp[$i][3]."</option>";
		}
		
		$adminExtra = $adminExtra."</select></div>";//roomstuff div
	}//end if(isAdmin)


/*echo "<pre>";var_dump($roomArray); echo "</pre>";*/

echo "
	<html lang='en'>";

echo "<head>
    <meta charset='utf-8'/>
      <meta http-equiv='X-UA-Compatible' content='IE=edge'/>
      <meta name='viewport' content='width=device-width, initial-scale=1'/>
      <meta name='description' content=''/>
      <meta name='author' content=''/>
      <link rel='icon' href='../../favicon.ico'/>
      <title>Nest Apartment Manager</title>
      <link href='css/bootstrap.min.css' rel='stylesheet'/>
      <link href='css/navbar.css' rel='stylesheet'/>
  </head>";

  echo "<body>

      <div class='container'>
        <nav class='navbar navbar-default'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                  <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                  </button>
                  <a class='navbar-brand' href='home.php'>Nest Apartment Manager</a>
                </div>
                <div id='navbar' class='navbar-collapse collapse'>
                  <ul class='nav navbar-nav'>
                    <li><a href='home.php'>Home</a></li>
                    <li class='active'><a href='usage.php'>Usage</a></li>
                    <li><a href='logs.php'>Logs</a></li>
                  </ul>

                  <ul class='nav navbar-nav navbar-right'>
                    <li><p class='navbar-text'>$myID</p></li>
                    <li><a href='./settings.php'>Settings</a></li>
                    <li><a href='./logout.php'>Logout</a></li>
                  </ul>

                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
          </nav>

        <!-- Main component for a primary marketing message or call to action -->
          <div class='jumbotron' style='padding:20px;margin:10px;'>
            <center><h4>Nest Thermostat Graphs</h4></center>
          <center><h5>Tip: <?php echo getTip(); ?></h5></center>
          <br>
          <div class='row'>
      
            <div class='col-md-3'>$adminExtra
            <br>
            <div id='printHere'></div>

            <label for='data'>Graph Mode</label><br>
            <select class='btn btn-default' id='data' name='data'> 
              <option disabled hidden value='disabled'>Graph mode</option>
              <option value='current_temp' selected>Temperature</option>
              <option value='humidity'>Humidity</option>
              <option value='other'>Leaf Count, Etc.</option>
            </select>
    
            <br><br>
    
            <label for='time_interval'>Time Scale</label><br>
            <select class='btn btn-default' id='time_interval' name='time_interval'>
              <option disabled hidden value='disabled'>Scale</option>
              <option value='hourly' selected=''>Hourly</option>
              <option value='weekly'>Weekly</option>
              <option value='monthly'>Monthly</option>
            </select>

            <br><br>

            <input class='btn btn-primary' id='graph' type='button' value='Graph' onclick='loadData()'><br>
          </div><!--col-md-3-->

          <div class='col-md-9' style='height: '100%';'>
            <div id='linechart_material'></div>
          </div><!--col-md-9-->

        </div><!--row-->
      


          <h2>
            
 
            <script type='text/javascript' src='https://www.google.com/jsapi'></script>
              
            <script type='text/javascript'>

            roomArray=".json_encode($roomArray).";";

            echo " var graph_number = 2;
              var returned_data ='';
              var x_header = 'Date'; //this value won't change
              var column1 = 'Item 1'; //this value will be automatically generated
              var column2 ='Item 2'; //this value will be automatically generated
              var column3 ='Item 3';
              var title = 'Humidity Data from Past 24 hours'; //Need to figure out what I wnat heres
              var subtitle ='';
              google.load('visualization', '1.1', {packages: ['line','corechart', 'bar']});
              
              window.addEventListener('resize', resize, false);
              
              window.onload=function(){ loadData(); }
              function resize(){ loadData(); }

              //google.setOnLoadCallback(drawChart); // dont mess with this


                 ";


             echo "

             function drawChart() {
                    
              var graphMe = new google.visualization.DataTable();

              graphMe.addColumn('string', x_header);
              graphMe.addColumn('number', column1);

              if(graph_number==3){
                graphMe.addColumn('number', column3);
              }

              graphMe.addColumn('number', column2);
              graphMe.addRows(returned_data);


              var options = 
              {
                height: 300,
                width: '90%',
                chart:
                {
                  title: title,
                  subtitle: subtitle
                }//end chart
                  };//end options
                    
              //var options2 = google.charts.Line.convertOptions(options);
              var chart = new google.charts.Line(document.getElementById('linechart_material'));
              chart.draw(graphMe, options);

                }//End drawChart()
               </script>";


				echo " 

				<script type='text/javascript'>
                
                
                 
                  function drawBar() {
                  
                      
                      //google.load('visualization', '1', {packages: ['corechart', 'bar']});
                        /*var data1 = google.visualization.arrayToDataTable([
                        ['City', '2010 Population', '2000 Population'],
                        ['New York City, NY', 8175000, 8008000],
                        ['Los Angeles, CA', 3792000, 3694000],
                        ['Chicago, IL', 2695000, 2896000],
                        ['Houston, TX', 2099000, 1953000],
                        ['Philadelphia, PA', 1526000, 1517000]
                      ]);*/
                    var data1 = google.visualization.arrayToDataTable(returned_data);
                    //var data1 = new google.visualization.DataTable();

                    //data1.addColumn('string', x_header);
                  
                      
                      var options = 
                      {
                        title: title,
                          chartArea: {left:'15%',width: '70%'},
                          hAxis: 
                          {
                            title: 'Percent On',
                            minValue: 0,
                            maxValue:100,
                            textStyle: 
                            {
                    bold: true,
                    fontSize: 12,
                    color: '#4d4d4d'
                            },//end textStyle
                            
                            titleTextStyle: 
                            {
                              bold: true,
                              fontSize: 12,
                              color: '#4d4d4d'
                            }//end titleTextStyle
                          },
                          vAxis: 
                          {
                            textStyle: 
                            {
                                fontSize: 14,
                                bold: true,
                                color: '#848484'
                            }//end textStyle
                          },//end vAxis
                        
                          bar: {groupWidth: '70%'},
                          legend: { position: 'top', maxLines: 3 }
                      };//end options
                      
                      var chart = new google.visualization.BarChart(document.getElementById('linechart_material'));
                    
                      chart.draw(data1, options);
                  }//end drawBar
              </script>
                       </h2>
        
          
        
          </div><!--jumbotron-->
    
  
    </div> <!-- /container -->";



    echo "   <script type ='text/javascript'>
      
      function loadData() {
        
        var time_interval =document.getElementById('time_interval').value;
        var category =document.getElementById('data').value;
        var therm_id = document.getElementById('roomChange').value;
        var therm_id2;
        var valid = 1;
        var room2 = document.getElementById('roomChange1').value;
      


        if( room2 != 'disabled'){
          therm_id2= room2;
          graph_number = 3;
        }else{
          graph_number=2;
        }//if-else room2 NOT disabled

        if(therm_id =='disabled'){
          valid = 0;
          alert('Please select a room');
        }//if therm_id disabled

        if(category== 'disabled'){
          valid = 0;
          alert('Please select a graph mode');
        }else if(time_interval == 'disabled'){
          valid = 0;
          alert('Please select a scale');
        }//if category disabled elseif timer_interval disabled

        if(valid){
          $.ajax({url: 'graph.php', data: {therm_id : therm_id, category: category, time_interval: time_interval, therm_id2: therm_id2}, success:function(result){
              //on success, load the values into the pics
          	//alert('ready, graph!');
            alert(result);
              var values = jQuery.parseJSON(result);

              //I THINK values can be graphed now; data wise.

              returned_data = values;
              var titlePrefix = '';

              switch(category){

                case 'current_temp':
                  column1 = roomArray[therm_id] + ' Temperature';
                  column3= roomArray[therm_id2] + ' Temperature';
                  column2='Outside Temperature';
                  titlePrefix = 'Outside Temperature';
                  subtitle = 'In Fahrenheit';
                  break;
                
                case 'humidity':
                  column1= roomArray[therm_id] + ' Humidity';
                  column3= roomArray[therm_id2] + ' Humidity';
                  column2='Outside Humidity';
                  titlePrefix = 'Outside Humidity';
                  subtitle = 'As a Percentage';
                  break;
                
                case 'other':
                  column1=roomArray[therm_id] + ' Data';
                  column3=roomArray[therm_id2] + ' Data';
                  column2='';
                  titlePrefix = 'Misc.';
                  break;
        
                default:
                  column1='';
                  column2='';
                  column3='';
                  title='';
                  subtitle='';
                  break;
              }//end switch(category)

              switch(time_interval){

                case 'weekly':
                    title = titlePrefix + ' Data from Past 7 Days';
                    break;
                
                case 'hourly':
                  title = titlePrefix + ' Data from Past 24 Hours';
                  break;
                
                case 'monthly':
                  title = titlePrefix + ' Data from Past 30 Days';
                  break;

                default:
                  title = '';
                  subtitle= '';
                  break;
              }//end switch(time_interval)



              if(category != 'other'){         
                drawChart();
              } else{

                returned_data[0][1]=roomArray[therm_id];
                
                if(therm_id2!=null){
                  returned_data[0][2]=roomArray[therm_id2]; 
                }
                
                drawBar();  
              }//if-else(category != other)
            
            }//end function(result)
          });//END $.AJAX(...)

        }//end if(valid)
      }//end loadData()
    </script>
  </body>

  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>
    <script src='./js/bootstrap.min.js'></script>

</html>";




?>


