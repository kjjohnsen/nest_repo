<?php
/*ini_set("display_errors","on");
  error_reporting(E_ALL);
  */
  error_reporting(0);

  require_once('setup.php');
  /*Global Variables*/
  $confirmationCode = "";
  $hasRun = false;
  

?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Nest Apartment Manager</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body>
        <h1>Nest Apartment Manager</h1>

<?php
  connect();
  if(isset($_GET)&& $_GET == null){
  
  }else{
    $confirmationCode = isset($_GET['confirmationCode']) ? $_GET['confirmationCode'] : "" ;
    getInfo();
    addResident();
  }
?>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
  </html>

<?php

  function getInfo(){
    global $mysqli,$confirmationCode,$MyID,$buildingName,$room,$hasRun;
    $table = "nest_confirmation";
    
    $query = "SELECT * FROM $table WHERE confirmation_code='$confirmationCode'";

    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_ASSOC);
    $MyID = $row['myid'];
    $buildingName = $row['building_name'];
    $room = $row['room_number'];

    $hasRun = true;
  }


  function addResident(){
    global $mysqli,$MyID,$buildingName,$room,$hasRun;
    
    $table = "nest_resident_account";
    $query = "INSERT INTO $table (isManager,building_name,room_number,MyID)
    VALUES('FALSE','$buildingName',$room,'$MyID')";
    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    //$row = mysqli_fetch_array($results, MYSQL_NUM);
    emailResident();
  }

  function emailResident(){
    global $MyID,$home_dir;

    $to      = $MyID."@uga.edu";
    $subject = '[NEST ACCOUNT ACTIVATED]';
    $message = "
    Your nest account has been activated. Please login at $home_dir"."index.php using your UGA MyID login information.
    ";
    $headers = 'From: auto.nest4920@gmail.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
  }
  
?>