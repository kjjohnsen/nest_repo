<?php
/*DO NOT ADD HTML COMMENTS TO THIS PAGE OR IT WON'T FUNCTION PROPERLY*/
//session_start();
	
	//ini_set("display_errors","on");
 	//error_reporting(E_ALL);
 	
error_reporting(0);

require_once('setup.php');
checkLogin($_SESSION['isValid']);
date_default_timezone_set('America/Montreal'); 
require_once('nest.class.php');
connect();


$parse_amount = 15;

/*GET Variables*/
$therm_id = $_GET['therm_id'];
$therm_id2 = !empty($_GET['therm_id2']) ? $_GET['therm_id2'] : null;
$time_interval = $_GET['time_interval'];
$category = $_GET['category'];
$aggregate = 'false';

switch($category){

	case "current_temp":
	$outside_category ='outside_temp';
	break;
	case "humidity":
	$outside_category ="outside_humidity";
	break;
	default:
	$outside_category = null;
}

//need to find out what data im dealing with before doing this-> is therm_id set? what is the outside category?

//These steps stay the same. Not dependent on query

if($aggregate != 'true'){
	$credentials = array();
	$credentials = getCredentials($therm_id);
	$username = $credentials[0];
}

$limit = getLimit($time_interval);



if($aggregate=='true' && $category=='other'){
	$val = getAggregateBarGraph($limit);
	$var = json_encode($val);
	echo $var;

}
//Single graph for temperature or humidity.
else if(is_null($therm_id2)  &&  ($category == 'current_temp' || $category =='humidity')){
	$val = getSingleGraph($username,$category, $outside_category, $limit);
	$val = getSingleAverage($val);
	$var = json_encode($val);
	echo $var;
}
//double graph for temperature or humidity
else if(!is_null($therm_id2)  &&  ($category == 'current_temp' || $category =='humidity')){
	$credentials = array();
	$credentials = getCredentials($therm_id2);
	$username2 = $credentials[0];
	$val = getDoubleGraph($username, $username2,$category,$outside_category,$limit);
	$val = getDoubleAverage($val);
	$var =json_encode($val);
	echo $var;
}
else if(is_null($therm_id2) && $category='other'){
	$val = getSingleBarGraph($username,$category, $limit);
	$var = json_encode($val);
	echo $var;
}
else if($category ='other'){
	$credentials = array();
	$credentials = getCredentials($therm_id2);
	$username2 = $credentials[0];
	$val = getDoubleBarGraph($username, $username2,$category,$limit);
	$var =json_encode($val);
	echo $var;
}

//single graph for leafs, away, heat, fan

//double graph for leafs, away, heat, fan




function getLimit($time_interval){
	global $parse_amount;

	//echo '\nRUNNING getLimit FUNCTION\n';
	//"Weekly" -> Past week = 7days = 168 hrs = 10080mins
	//"Hourly" -> past day = 24 hrs = 1440 mins //
	//"Monthly" -> Past month = 30days = 720hrs = 43200mins
	switch ($time_interval){

		case "monthly":
		$parse_amount = 450;
		return 43200;
		break;
		$parse_amount = 15;
		case "hourly":
		return 1440;
		break;
		default:
		$parse_amount= 105;
		return 10080;
	}
}

function getSingleAverage($ar){
	global $parse_amount;

	//echo '\nRUNNING getSingleAverage FUNCTION\n';
	$new_array = array();
	$average = 0;
	$average1= 0;
	$count = 0;
	for($i = 0; $i<sizeof($ar); $i++){
		$average += $ar[$i][1]; //temperature
		$average1 +=$ar[$i][2]; //outside temperature
		if(($i+1) % $parse_amount == 0){
			$average = $average / $parse_amount;
			$average1= $average1 / $parse_amount;
			//echo"now assigning new_array at [".$count."][0]equal to ".$ar[($i-$parse_amount/2)][0]."<br>";
			//echo"now assigning new_array at [".$count."][1]equal to ".$average."<br>";
			$new_array[$count][0] = $ar[($i-round($parse_amount/2))][0];
			$new_array[$count][1] = $average;
			$new_array[$count][2] = $average1;
			$average = 0;
			$average1=0;
			$count++;
		}
		
	}
	return $new_array;
}
function getDoubleAverage($ar){
	global $parse_amount;
	$new_array = array();
	$average = 0; //timestamp
	$average1= 0; //resident data 1
	$average2= 0;//resident data 2
	$count = 0;
	for($i = 0; $i<sizeof($ar); $i++){
		$average += $ar[$i][1]; // temp 1
		$average1 +=$ar[$i][2]; // temp 2
		$average2 +=$ar[$i][3]; // outside temp
		if(($i+1) % $parse_amount == 0){
			$average = $average / $parse_amount;
			$average1= $average1 / $parse_amount;
			$average2= $average2/ $parse_amount;	
			//echo"now assigning new_array at [".$count."][0]equal to ".$ar[($i-$parse_amount/2)][0]."<br>";
			//echo"now assigning new_array at [".$count."][1]equal to ".$average."<br>";
			$new_array[$count][0] = $ar[($i-round($parse_amount/2))][0];
			$new_array[$count][1] = $average;
			$new_array[$count][2] = $average1;
			$new_array[$count][3] = $average2;
			$average = 0;
			$average1=0;
			$average2=0;
			$count++;
		}
		
	}
	return $new_array;
}

function getDoubleGraph($username,$username2,$category,$outside_category,$limit){
	global $mysqli;
	//echo '\nRUNNING getDoubleGraph FUNCTION\n';
	$data = array();
	$query="SELECT timestamp, ".$category. ", ".$outside_category. " FROM nest_datalog WHERE nest_account_username='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query2="SELECT timestamp, ".$category. ", ".$outside_category. " FROM nest_datalog WHERE nest_account_username='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$results =$mysqli->query($query) or die(mysqli_error($mysqli));
	$results2 =$mysqli->query($query2) or die(mysqli_error($mysqli));
	$i = 0;
	while(  ($row=mysqli_fetch_array($results,MYSQL_NUM)) && ($row2=mysqli_fetch_array($results2,MYSQL_NUM))  ){
		$row[0]= date("M d, Y \nh:i", $row[0]);
		$row[1]= 0+$row[1];
		$row[3]= 0+$row[2];
		$row[2]= 0+$row2[1];
		$data[$i] = $row;
		$i++;
	}
	return array_reverse($data);

}
function getSingleGraph($username,$category,$outside_category,$limit){
	global $mysqli;
	//echo '\nRUNNING getSingleGraph FUNCTION\n';
	$data = array();
	$query="SELECT timestamp, ".$category. ", ".$outside_category. " FROM nest_datalog WHERE nest_account_username='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$results =$mysqli->query($query) or die(mysqli_error($mysqli));
	$i= 0;
	while($row=mysqli_fetch_array($results,MYSQL_NUM)){
		$row[0]= date("M d, Y \nh:i", $row[0]);
		$row[1]= 0+$row[1];
		$row[2]= 0+$row[2];
		$data[$i] = $row;
		$i++;
	}
	return array_reverse($data);
}
function getSingleBarGraph($username,$category,$limit){
	global $mysqli;
	//echo '\nRUNNING getSingleBarGraph FUNCTION\n';
	$data = array();
	$q_suffix = " FROM nest_datalog WHERE nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;

	$query = "SELECT is_away".$q_suffix;
	$query2 ="SELECT is_heat".$q_suffix;
	$query3 = "SELECT is_leaf".$q_suffix;
	$query4 = "SELECT fan_is_on".$q_suffix;

	$results =$mysqli->query($query) or die(mysqli_error($mysqli));
	$results2 =$mysqli->query($query2) or die(mysqli_error($mysqli));
	$results3 =$mysqli->query($query3) or die(mysqli_error($mysqli));
	$results4 =$mysqli->query($query4) or die(mysqli_error($mysqli));

	$counter1 =0; 
	$counter2= 0;
	$counter3= 0;
	$counter4=0;
	while($row=mysqli_fetch_array($results,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter1++;
		}
	}
	while($row=mysqli_fetch_array($results2,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter2++;
		}
	}
	while($row=mysqli_fetch_array($results3,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter3++;
		}
	}
	while($row=mysqli_fetch_array($results4,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter4++;
		}
	}
	
	
	$data[0][0] ="Time (minutes)";
	$data[0][1] ="Room 1";
	$data[1][0] ="Away";	
	$data[1][1] = $counter1;
	$data[2][0] ="Heat";
	$data[2][1] = $counter2;
	$data[3][0] ="Leaf";
	$data[3][1] = $counter3;
	$data[4][0] ="Fan";
	$data[4][1] = $counter4;


	$data[1][1] = 100*$data[1][1]/$limit;
	$data[2][1] = 100*$data[2][1]/$limit;
	$data[3][1] = 100*$data[3][1]/$limit;
	$data[4][1] = 100*$data[4][1]/$limit;
	return $data;
}
function getAggregateBarGraph($limit){
	global $mysqli;
	//echo '\nRUNNING getAggregateBarGraph FUNCTION\n';
	$data = array();

	$query = "SELECT SUM(is_away) FROM (SELECT * FROM nest_datalog WHERE is_away='TRUE' ORDER BY id DESC LIMIT ". $limit .") AS test";
	$query2 = "SELECT SUM(is_heat) FROM (SELECT * FROM nest_datalog WHERE is_heat='TRUE' ORDER BY id DESC LIMIT ". $limit .") AS test";
	$query3 = "SELECT SUM(is_leaf) FROM (SELECT * FROM nest_datalog WHERE is_leaf='TRUE' ORDER BY id DESC LIMIT ". $limit .") AS test";
	$query4 = "SELECT SUM(fan_is_on) FROM (SELECT * FROM nest_datalog WHERE fan_is_on='TRUE' ORDER BY id DESC LIMIT ". $limit .") AS test";
	$results =$mysqli->query($query) or die(mysqli_error($mysqli));
	$results2 =$mysqli->query($query2) or die(mysqli_error($mysqli));
	$results3 =$mysqli->query($query3) or die(mysqli_error($mysqli));
	$results4 =$mysqli->query($query4) or die(mysqli_error($mysqli));

	$row=mysqli_fetch_array($results,MYSQL_NUM);
	$row2=mysqli_fetch_array($results2,MYSQL_NUM);
	$row3=mysqli_fetch_array($results3,MYSQL_NUM);
	$row4=mysqli_fetch_array($results4,MYSQL_NUM);
	
	$data[0][0] ="Time (minutes)";
	$data[0][1] ="Aggregate Data";
	$data[1][0] ="Away";	
	$data[1][1] = 0 +$row[0];
	$data[2][0] ="Heat";
	$data[2][1] = 0+ $row2[0];
	$data[3][0] ="Leaf";
	$data[3][1] = 0+ $row3[0];
	$data[4][0] ="Fan";
	$data[4][1] = 0+ $row4[0];

	$data[1][1] = 100*$data[1][1]/$limit;
	$data[2][1] = 100*$data[2][1]/$limit;
	$data[3][1] = 100*$data[3][1]/$limit;
	$data[4][1] = 100*$data[4][1]/$limit;



	return $data;
}

/*This could be more elegant*/
function getDoubleBarGraph($username,$username2,$category,$limit){
	global $mysqli;
	//echo '\nRUNNING getDoubleBarGraph FUNCTION\n';
	$data = array();

	$query = "SELECT is_away FROM nest_datalog WHERE nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query2 ="SELECT is_heat FROM nest_datalog WHERE nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query3 = "SELECT is_leaf FROM nest_datalog WHERE nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query4 = "SELECT fan_is_on FROM nest_datalog WHERE nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;

	$query5 = "SELECT is_away FROM nest_datalog WHERE nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$query6 ="SELECT is_heat FROM nest_datalog WHERE nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$query7 = "SELECT is_leaf FROM nest_datalog WHERE nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$query8 = "SELECT fan_is_on FROM nest_datalog WHERE nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;

	$results =$mysqli->query($query) or die(mysqli_error($mysqli));
	$results2 =$mysqli->query($query2) or die(mysqli_error($mysqli));
	$results3 =$mysqli->query($query3) or die(mysqli_error($mysqli));
	$results4 =$mysqli->query($query4) or die(mysqli_error($mysqli));
	$results5=$mysqli->query($query5) or die(mysqli_error($mysqli));
	$results6 =$mysqli->query($query6) or die(mysqli_error($mysqli));
	$results7 =$mysqli->query($query7) or die(mysqli_error($mysqli));
	$results8 =$mysqli->query($query8) or die(mysqli_error($mysqli));

	$counter1 =0; 
	$counter2= 0;
	$counter3= 0;
	$counter4=0;
	$counter5 = 0;
	$counter6= 0;
	$counter7=0;
	$counter8=0;
	while($row=mysqli_fetch_array($results,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter1++;
		}
	}
	while($row=mysqli_fetch_array($results2,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter2++;
		}
	}
	while($row=mysqli_fetch_array($results3,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter3++;
		}
	}
	while($row=mysqli_fetch_array($results4,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter4++;
		}
	}
	while($row=mysqli_fetch_array($results5,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter5++;
		}
	}
	while($row=mysqli_fetch_array($results6,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter6++;
		}
	}
	while($row=mysqli_fetch_array($results7,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter7++;
		}
	}
	while($row=mysqli_fetch_array($results8,MYSQL_NUM)){
		if($row[0] == 'TRUE'){
			$counter8++;
		}
	}	
	
	$data[0][0] ="Time (minutes)";
	$data[0][1] ="Room 1";
	$data[0][2] = "Room 2";
	$data[1][0] ="Away";	
	$data[1][1] = $counter1;
	$data[1][2] = $counter6;
	$data[2][0] ="Heat";
	$data[2][1] = $counter2;
	$data[2][2] = $counter6;
	$data[3][0] ="Leaf";
	$data[3][1] = $counter3;
	$data[3][2] = $counter7;
	$data[4][0] ="Fan";
	$data[4][1] = $counter4;
	$data[4][2] = $counter8;

	$data[1][1] = 100*$data[1][1]/$limit;
	$data[1][2] = 100*$data[1][2]/$limit;
	$data[2][1] = 100*$data[2][1]/$limit;
	$data[2][2] = 100*$data[2][2]/$limit;
	$data[3][1] = 100*$data[3][1]/$limit;
	$data[3][2] = 100*$data[3][2]/$limit;
	$data[4][1] = 100*$data[4][1]/$limit;
	$data[4][2] = 100*$data[4][2]/$limit;


	return $data;
}


/*	$query = "SELECT COUNT(is_away) FROM nest_datalog WHERE is_away='TRUE' AND nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query2 = "SELECT COUNT(is_heat) FROM nest_datalog WHERE is_heat='TRUE' AND nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query3 = "SELECT COUNT(is_leaf) FROM nest_datalog WHERE is_leaf='TRUE' AND nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;
	$query4 = "SELECT COUNT(fan_is_on) FROM nest_datalog WHERE fan_is_on='TRUE' AND nest_account_username ='".$username."' ORDER BY id DESC LIMIT ". $limit;

	$query5 = "SELECT COUNT(is_away) FROM nest_datalog WHERE is_away='TRUE' AND nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$query6 = "SELECT COUNT(is_heat) FROM nest_datalog WHERE is_heat='TRUE' AND nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$query7 = "SELECT COUNT(is_leaf) FROM nest_datalog WHERE is_leaf='TRUE' AND nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;
	$query8 = "SELECT COUNT(fan_is_on) FROM nest_datalog WHERE fan_is_on='TRUE' AND nest_account_username ='".$username2."' ORDER BY id DESC LIMIT ". $limit;*/
?>