<?php
/*ini_set("display_errors","on");
  error_reporting(E_ALL);
  */
  error_reporting(0);

  require_once('setup.php');
  /*Global Variables*/
	$MyID ="";
	$userPassword="";
	$hasRun = false;
	$login_result;
	$isValid = false;
	$isAdmin = false;
	$residentID = -1;
	$room = "";
	$buildingName = "";
	$thermostatAccountID = -1;
  $managerMyID = "";
  $confirmationCode = "";


?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Nest Apartment Manager</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body>
        <h1>Nest Apartment Manager</h1>

<?php


  /*MAIN METHOD*/
  connect();

  $info = loginDialogue();
  if($hasRun){
    $isValid = validate($info[0], $info[1]);
   /*
    echo $login_result;
    echo '<br>';
    echo '<a class="btn btn-lg btn-block btn-primary" style="width:150px" href="./index.php" role="button">Back to Login</a>';
        */
  
    switch($isValid){
      case 0: 
        echo '<script type="text/javascript">
        alert("Invalid login");
        window.location.replace("./signup.php");
        </script> ';
        break;
      case 1:
        echo '
        <script type="text/javascript">
         window.location.replace("./index.php");
        </script> ';
        break;
      case 2:
        echo '
        <script type="text/javascript">
        alert("The MyID you entered already has an account associated with it.");
         window.location.replace("./index.php");
        </script> ';
        break;
    }
  }
?>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
  </html>


<?php


  //Returns an array with the credentials 

  function LoginDialogue(){
    global $login_result, $MyID, $userPassword, $hasRun, $isValid,$buildingName,$room;
    /*Unit Testing for Validate function*/
    if(isset($_POST)&& $_POST == null){

      echo'
          <h2 class="text-center">Sign up using MyID to access your Nest Thermostat</h2>
      		
          <div class="container">
      			<form method="POST" class="form-signin">

      				<input type="text" name="MyID" id="inputEmail" class="form-control" placeholder="MyID" required autofocus>
      				<label for="inputPassword" class="sr-only">Password</label>
      				<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
      				<br>
              <div class="row">
                <div class="form-group" height=20>
    						  <div class="col-lg-6 col-md-6 col-sm-12">
    								<select class="form-control" style="height: 50px;" name="buildingName" required>
    									<option value="" disabled selected invalid>(Select Building)</option>
    									<option value="Building_U">Building U</option>
                      <option value="Building_V">Building V</option>
    								</select>	
    						  </div>
                 
    						  <div class="col-lg-5 col-md-5 col-sm-12">
                    <input type="number" id="roomNumber" name="room" placeholder="Room#" size="4" required>    
                  </div>
        			   </div>
              </div>
      					
              <br>
      				
              <input type="submit" name="Submit" value="Sign up" class="btn btn-lg btn-block btn-primary"  role="button">
            </form>
          </div>'; // <!-- /container -->

    }

    else{
      $hasRun = true;
      $MyID = isset($_POST['MyID']) ? $_POST['MyID'] : "" ;
      $userPassword = isset($_POST['password']) ? $_POST['password'] : "" ;
      $buildingName = isset($_POST['buildingName']) ? $_POST['buildingName'] : "" ;
      $room = isset($_POST['room']) ? $_POST['room'] : "" ;
      //echo $buildingName." ".$room;


      $credentials = array();
      $credentials[0] = $MyID;
      $credentials[1] = $userPassword;
      return $credentials;
    }
  }

  function validate($MyID, $password){
    global $login_result, $resident_table, $isAdmin;
    $ldaprdn = 'myid' . "\\" . $MyID;
    $domain = "msmyid.uga.edu";
    $ldap = ldap_connect($domain) 
    or die("Couldn't connect to Active Directory");
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION,3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS,0);

    if(strlen($MyID) === 0){
      $login_result = "Username cannot be empty"; 
      return 0;
    }
    else if(strlen($password) === 0){
      $login_result = "Password cannot be empty"; 
      return 0;
    }
    if ($bind = ldap_bind($ldap, $ldaprdn, $password)) {
    // If the bind is successful, that means it is a valid myID
    //Check to see if the MYID is in OUR table.

      if(lookUpMyID($_POST['MyID'],$resident_table)){
        $login_result="<br>Account for ".$_POST['MyID']." already exists!";
        //getInfo($_POST['MyID'],$resident_table);
        return 2;
      }
      else{
      	//addResident();
        generateAndSendCode();
       	$login_result="SUCCESS! You will receive a confirmation email once your request has been approved.";
        return 1;
      }

    }
    else{
      $login_result = "<br>Bad Username or Password<br>";
      return 0;
    }
  }

  /*Looks up the MyID in the table. Used in conjunction w/ validate function*/
  function lookUpMyID($MyID, $table){
    global $mysqli;
    $MyID = $mysqli->escape_string($MyID);
    $query = "SELECT COUNT(DISTINCT MyID) FROM $table WHERE MyID='$MyID'";

    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_NUM);

    return$row[0] >=1;

  }


  function generateAndSendCode(){
    lookupManagerMyID();
    generateConfirmationCode();
    sendEmail();
  }


  function lookupManagerMyID(){
    global $mysqli,$managerMyID,$buildingName;
    $table = "nest_building_manager";
    $query = "SELECT MyID FROM $table WHERE building_name='$buildingName'";

    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_NUM);
    $managerMyID = $row[0];
    
  }

  function generateConfirmationCode(){
    global $mysqli,$MyID,$managerMyID,$buildingName,$room,$confirmationCode;
    $table = "nest_confirmation";
    $confirmationCode = md5(uniqid(rand()));
    //$query = "SELECT MyID FROM $table WHERE building_name='$buildingName'";
    $query = "INSERT INTO $table (manager_id,myid,confirmation_code,building_name,room_number)
    VALUES('$managerMyID','$MyID','$confirmationCode','$buildingName','$room')";
    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    //$row = mysqli_fetch_array($results, MYSQL_NUM);
    

  }

  function sendEmail(){
    global $MyID,$managerMyID,$buildingName,$room,$confirmationCode,$home_dir;
    $to      = $managerMyID."@uga.edu";
    $subject = '[APPROVE NEST ACCOUNT]';
    $message = "
    Please verify the following:
            MyID:     '$MyID'
            Building: '$buildingName'
            Room#:    '$room'
    ";
    $message = $message."

    Click on the following to confirm: $home_dir"."confirm.php?confirmationCode=$confirmationCode";
    $headers = 'From: auto.nest4920@gmail.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
  }

?>