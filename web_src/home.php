<?php
  session_start();
/*ini_set("display_errors","on");
  error_reporting(E_ALL);
  */
  error_reporting(0);

  require_once('setup.php');
  checkLogin($_SESSION['isValid']);

?>

<html>
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap core CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/navbar.css" rel="stylesheet">
    
  </head>


  <body>    
    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">

        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Nest Apartment Manager</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="usage.php">Usage</a></li>
              <li><a href="logs.php">Logs</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><p class="navbar-text"><?php echo $_SESSION['MyID']; ?></p></li>
              <li><a href='./settings.php'>Settings</a></li>
              <li><a href="./logout.php">Logout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->

      </nav>



        <!--dynamically sized select box-->
        <div id="test">
          <?php
            if($_SESSION['isAdmin']){
              $temp = getRooms();  
              echo "<label for='roomChange'>Select Room to Control  </label>
              
              <select id='roomChange' name='roomChange'>
              ";
              for($i = 0; $i<sizeof($temp); $i++){
                $result = str_replace('_'," ",$temp[$i][2]);
                echo"<option value='".$temp[$i][4]."'>".$result." (".$temp[$i][3].")</option>";
              }
              echo "</select><br><br>";
            }
          ?>
        </div><!--end of test-->
        <div id="nest_ctrl">

        <!--CONTAINER FLUID-->
        <div class="container-fluid" style="background:#eee; margin: 0px; padding: 0px;">
          
          <form style="margin: 0px; padding: 0px;">

            <div class="row" style="margin: 0px; padding: 0px;">

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-LEFT: 0px; padding: 0px;">

                <div style="margin: 0px;padding: 0px;">
                    

                    <!--THERMOSTAT IMAGE THING-->
                    <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-1 col-md-5 col-md-offset-2 col-lg-4 col-lg-offset-3" style="margin: 0px;padding:0px;">

                      <canvas id="canvas" style="margin: 0px;padding:0px;">
                        
                        This text is displayed if your browser does not support HTML5 Canvas.
                     
                      </canvas>

                    </div><!--class="col-xs-12 col-sm-8 col-md-6 col-lg-7"-->

                    <div class="col-xs-12 col-xs-offset-0 col-sm-5 col-sm-offset-0 col-md-5 col-md-offset-0 col-lg-5 col-lg-offset-0" style="margin: 0px;">
                     
                     <div class="form-group"></br></div>

                      <div class="form-group">
                        <label for="awayGroup" style="font-size:150%;"> Away Mode</label>
                        <div class="form-group" id="awayGroup">

                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="away_on" name="away" value="on">On
                          </label>
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="away_off" name="away" value="off">Off  
                          </label>
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="away_auto" name="away" value="auto">Auto  
                          </label>
                        </div><!--awayGroup-->
                      </div><!--form-group-->

                      <div class="form-group"><!--formgroup2-->
                        <label for="fanGroup" style="font-size:150%;"> Fan Mode</label>
                        <div class="form-group" id="fanGroup">
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="fan_on" name="fan" value="on">On
                          </label>
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="fan_off" name="fan" value="off">Off
                          </label>
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="fan_auto" name="fan" value="auto">Auto  
                          </label>
                            
                          </div><!--fangroup-->
                      </div><!--formgroup2-->

                      <div class="form-group"><!--formgroup3-->
                        <label for="heatGroup" style="font-size:150%;">Heat/AC</label>
                        <div class="form-group" id="heatGroup"><!--heatgroup-->
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="mode_heat" name="mode" value="heat">Heat  
                          </label>
                          
                          <label class="radio-inline" style="font-size:120%;">
                            <input type="radio" id="mode_cool" name="mode" value="cool">Cool
                          </label> 
                        
                        </div><!--heatgroup-->
                      </div><!--formgroup3-->
                      
                      <div id="types" class="btn-group">
                        <button name="color" type="button" class="btn btn-success"  value="btn-success"  onclick="DoStuff('set')">Set Nest
                        </button>
                      </div>

                    </div>

                </div>

              </div>

            </div>

          </form>
         
        </div>
        <!--container-fluid-->

      </div><!--has nest-->

      <div id="no_nest" style="display:none">
        YOUR NEST SEEMS TO BE DEACTIVATED. PLEASE CONTACT THE BUILDING MANAGER.
      </div><!--NO NEST-->



      <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 

    <script type="text/javascript">
      debug = true;
      var canvas;
      var ctx;
      var x = 160;
      var y = 165;
      var r1 = 150;
      var r2 = 100;
      var temp = 73;
      var isAway= false;
      var isHeating = false;
      var isCooling = false;
              



     //window.onload(test());
      // This function should be here since it references var temp
      function DoStuff(action){

        if(action == 'set'){

          if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
          } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          
          xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
              document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
              var test = xmlhttp.responseText;
        
            }

          } 

        }  
              
        string1 = "temp=" + temp;
        if(document.getElementById('away_on').checked){
          string2="away=on";
          isAway= true;

        }else if(document.getElementById('away_off').checked){
          string2="away=off";
          isAway = false;
        }else if(document.getElementById('away_auto').checked){
          string2="away=auto";
        }else{
          string2="away=null";
        }


        if(document.getElementById('fan_on').checked){
          string3="fan=on";
        }else if(document.getElementById('fan_off').checked){
          string3="fan=off";
        }else if(document.getElementById('fan_auto').checked){
          string3="fan=auto";
        }else{
          string3="fan=null";
        }


        if(document.getElementById('mode_cool').checked){
          string4="mode=cool";
          isCooling = true;
          isHeating = false;
        }else if(document.getElementById('mode_heat').checked){
          string4="mode=heat";
          isHeating= true;
          isCooling= false;
        }else{
          string4="mode=null";
        }

        var therm_id = "therm="+document.getElementById('roomChange').value;
        var combined_string = string1 + "&" + string2 + "&" +string3 + "&" +string4 + "&" +therm_id;

          
        if(action =='set'){
           xmlhttp.open("GET","set.php?"+combined_string,true);
           xmlhttp.send();
        }
        update();
              
      }//END DoStuff()  


      //Center of screen is 960 for me, but it changes when the window changes.

      canvas = document.getElementById('canvas');
      ctx = canvas.getContext('2d');

      window.addEventListener('resize', resizeCanvas, false);
      //window.addEventListener('orientationchange', resizeCanvas, false);
      
      canvas.addEventListener("mousedown", doMouseDown, false);

      var upArrow = new Image();
      upArrow.src = 'images/upArrow.png';
      var downArrow = new Image();
      downArrow.src = 'images/downArrow.png';
        
      upArrow.onload = function() {
        ctx.drawImage(upArrow, x-102, y-125-102);
      };
      
      downArrow.onload = function() {
        ctx.drawImage(downArrow, x-102, y+125-102);
      };

      window.onload = function(){
            resizeCanvas();
        //getData(null); //get the default users data when the page loads

      };
      
      function resizeCanvas(){
        
        canvas.width = Math.min(320,(window.innerWidth-20));
        canvas.height = Math.min(320,(window.innerHeight-20));

        var scaleX = canvas.width / 320, scaleY = canvas.height / 320;
        var scale = Math.min(scaleX,scaleY)*.9;
        ctx.scale(scale,scale);

        update();
      }//resizeCanvas()
      
      function doMouseDown(event){
        var canvas_x = event.x;
        var canvas_y = event.y;
        if (event.x != undefined && event.y != undefined)
        {
          canvas_x = event.x;
          canvas_y = event.y;
        }
        else // Firefox method to get the position
        {
          canvas_x = event.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
          canvas_y = event.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
        }

        var rect = canvas.getBoundingClientRect();
        canvas_x -= rect.left;
        canvas_y -= rect.top;

        if (canvas_x > x-70 && canvas_x < x+70){//check if click is within x values
          if(canvas_y > y-160 && canvas_y < y-50){
            changeTemp('increase');
          }
          else if(canvas_y > y+50 && canvas_y < y+160){
            changeTemp('decrease');
          }
        }
        
      }//doMouseDown(event)

      //var scaleX = window.innerWidth / 360, scaleY = window.innerHeight / 400;
      //ctx.scale(Math.min(1, Math.min(scaleX, scaleY)), Math.min(1, Math.min(scaleX, scaleY)));

      ctx.fillStyle = "grey";
      ctx.strokeStyle = "black";
      
      ctx.beginPath();
      ctx.arc(x, y, r1, 0, Math.PI*2, true);
      ctx.fill();
      

      if(isAway){
        ctx.fillStyle = "black";// changed to black- CWC
      }else if(isHeating){
        ctx.fillStyle = "orange";
      }else{
        ctx.fillStyle = "blue";
      }
      

      ctx.strokeStyle = "black";
      
      ctx.beginPath();
      ctx.arc(x, y, r2, 0, Math.PI*2, true);
      ctx.fill();   
      
      
      ctx.fillStyle = "white";
      ctx.font="80px Arial";
      ctx.textAlign="center";
      ctx.fillText(temp, x, y+30);

      changeTemp = function(change){
        if(change == 'increase' && (temp< 90)){
          temp++;
        }
        if(change == 'decrease' && (temp>50)){
          temp--;
        }
        update();
          
      };
      
      update = function(){
       
        ctx.fillStyle = "grey";
        ctx.strokeStyle = "black";
        
        ctx.beginPath();
        ctx.arc(x, y, r1, 0, Math.PI*2, true);
        ctx.fill();
        
        if(isAway){
          ctx.fillStyle = "black";
        }
        else if(isHeating){
          ctx.fillStyle = "orange";
        }
        else{
          ctx.fillStyle = "blue";
        }
      

        ctx.strokeStyle = "black";
        
        ctx.beginPath();
        ctx.arc(x, y, r2, 0, Math.PI*2, true);
        ctx.fill();   
                
        ctx.fillStyle = "white";
        ctx.font="80px Arial";
        ctx.textAlign="center";
        ctx.fillText(temp, x, y+30);
          
        ctx.drawImage(upArrow, x-102, y-125-102);
        ctx.drawImage(downArrow, x-102, y+125-102);
        
      };

       $(function(){

      getData();  

    });

    $("#roomChange").change(function(){

      getData();
      DoStuff('not_set');

    });

    function getData(){

      var therm_id = "q="+document.getElementById('roomChange').value;


      $.ajax({url: "lookup.php", data: therm_id, success: function(result){
        //on success, load the values into the pics


          var values = jQuery.parseJSON(result);
          if(values[0]==="NO_NEST"){
            //alert(therm_id);
            document.getElementById("nest_ctrl").style.display="none";
            document.getElementById("no_nest").style.display="inline";
            
            return;
          }else{

            document.getElementById("nest_ctrl").style.display="inline";
            document.getElementById("no_nest").style.display="none";
          }

           if(values[0] != null){
            temp = values[0];
           }
           else{ // temperature is null; set default to 73
            temp = 73;
           }

           if(values[1] != null){
              
              if(values[1].search(/heat/i) != -1){
                isCooling = false;
                isHeating = true;
                document.getElementById("mode_heat").checked = true;
              }else if(values[1].search(/cool/i) != -1){
                isCooling = true;
                isHeating = false;
                document.getElementById("mode_cool").checked = true;
              }

              if(values[1].search(/away/i) != -1){
                isAway=true;
              }
           } else{ //Values are null, set default to cooling
            isAway= false;
            isHeating = false;
            isCooling = true;
           }
           if(values[2] == true){
           document.getElementById("fan_on").checked = true;
           }
           else{
            document.getElementById("fan_off").checked = true;
           }
           if(values[3] == true){
           document.getElementById("away_on").checked = true;
           }
           else{
            document.getElementById("away_off").checked = true;
           }
           
           update();
        }});

    }

        
    </script>    
  </br>

  <div class="col-xs-4 col-sm-4"><div id="txtHint"><b></b></div></div>


  </body>
</html>