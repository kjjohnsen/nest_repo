<?php 
  session_start();
/*ini_set("display_errors","on");
  error_reporting(E_ALL);
  */
  error_reporting(0);


  require_once('setup.php');

  if(isset($_SESSION['isValid']) && $_SESSION['isValid'])
    {
      header("Location: ./home.php");
  }
  /*Global Variables*/
  $MyID ="";
  $userPassword="";
  $hasRun = false;
  $login_result;
  $isValid = false;
  $isAdmin = false;
  $residentID = -1;
  $room = "";
  $buildingName = "";
  $thermostatAccountID = -1;
  //$managerID = -1;
  $thermostatUsername = "";
  $thermostatPassword = "";

?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Nest Apartment Manager</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body>
        <h1>Nest Apartment Manager</h1>

<?php


  /*MAIN METHOD*/
  connect();

  $info = LoginDialogue();
  if($hasRun){
   
    $isValid = validate($info[0], $info[1]);

  
    if($isValid){
      $_SESSION['MyID'] = $mysqli->escape_string($MyID);
      $_SESSION['isValid'] = true;
      $_SESSION['isAdmin'] = $isAdmin;
      $_SESSION['thermostatUsername'] = $thermostatUsername;
      $_SESSION['thermostatPassword'] = $thermostatPassword;
      $_SESSION['thermostatAccountID'] = $thermostatAccountID;
      $_SESSION['room'] = $room;
      $_SESSION['buildingName'] = $buildingName;
      $_SESSION['opt_in'] = checkOptIn($room);
      $_SESSION['residentID'] = $residentID;
       echo '
      <script type="text/javascript">
       window.location.replace("./home.php");
      </script> ';

    }else{

      echo '<script type="text/javascript">
      alert("Invalid login");
      window.location.replace("./");
      </script> ';
    }
  }
?>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
  </html>


<?php


  //Returns an array with the credentials 

  function LoginDialogue(){
    global $login_result, $MyID, $userPassword, $hasRun, $isValid;
    /*Unit Testing for Validate function*/
    if(isset($_POST)&& $_POST == null){

      echo'
      <div class="container">

        <form method="POST" class="form-signin">

          <h2 class="form-signin-heading">Please log in</h2>
          <label for="inputEmail" class="sr-only">Email address</label>
          <input type="text" name="MyID" id="inputEmail" class="form-control" placeholder="MyID" required autofocus>
          <label for="inputPassword" class="sr-only">Password</label>
          <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>';
    /*<!-- Deactivating the real form links until we can make them work. For now, dummy buttons that just work as links
          <button href="./profile.html" class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
          <button href="./profile.html" class="btn btn-lg btn-primary btn-block" type="submit">Log in</button> -->*/
          echo' 
          <input type="submit" name="Submit" value="Log in" class="btn btn-lg btn-block btn-primary"  role="button">';
          if(!isset($_SESSION['isValid']) || !(($_SESSION['isValid']))){
            echo '<a class="btn btn-lg btn-block btn-primary" href="./signup.php" role="button">Sign up</a>';
          }
          echo'
        </form>
      </div>'; // <!-- /container -->

    }

    else{
      $hasRun = true;
      $MyID = isset($_POST['MyID']) ? $_POST['MyID'] : "" ;
      $userPassword = isset($_POST['password']) ? $_POST['password'] : "" ; 

      $credentials = array();
      $credentials[0] = $MyID;
      $credentials[1] = $userPassword;
      return $credentials;
    }
  }

  function validate($MyID, $password){
    global $login_result, $resident_table, $isAdmin;
    $ldaprdn = 'myid' . "\\" . $MyID;
    $domain = "msmyid.uga.edu";
    $ldap = ldap_connect($domain) 
    or die("BAD USERNAME/PASSWORD");
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION,3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS,0);

    if(strlen($MyID) === 0){
      $login_result = "Username cannot be empty"; 
      return false;
    }
    else if(strlen($password) === 0){
      $login_result = "Password cannot be empty"; 
      return false;
    }
    if ($bind = ldap_bind($ldap, $ldaprdn, $password)) {
    // If the bind is successful, that means it is a valid myID
    //Check to see if the MYID is in OUR table.

      if(lookUpMyID($_POST['MyID'],$resident_table)){
        $login_result="Login Sucessful";
        getInfo($_POST['MyID'],$resident_table);

        return true;
      }
      else{
        $login_result="Access not granted";
        return false;
      }

    }
    else{
      $login_result = "BAD USERNAME/PASSWORD";
      return false;
    }
  }

  /*Looks up the MyID in the table. Used in conjunction w/ validate function*/
  function lookUpMyID($MyID, $table){
    global $mysqli;
    $MyID = $mysqli->escape_string($MyID);
    $query = "SELECT COUNT(DISTINCT MyID) FROM $table WHERE MyID='$MyID'";

    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_NUM);

    if($row[0] >=1){
      return true;
    }
    return false;

  }
  function checkOptIn($room){
    global $mysqli;

    $query = "SELECT opt_in from nest_room_number_X_account where room_number=".$room;
    $results = $mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_NUM);
    return $row[0];
  }


//===================================================================//
 //ELLIOTT'S STUFF!!!!!

 /* $isAdmin = false;
  $residentID = -1;
  $room = "";
  $thermostatAccountID = -1;*/
  //GET EVERYTHING FROM NEST_RESIDENT_ACCOUNT TABLE
  function getInfo($MyID,$table){
    global $mysqli,$isAdmin,$residentID,$room,$buildingName,$managerID;
    $MyID = $mysqli->escape_string($MyID);
    $query = "SELECT * FROM $table WHERE MyID='$MyID'";

    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_ASSOC);
    $isAdmin = $row['isManager'];
    $isAdmin = ($isAdmin=='TRUE');
    $room = $row['room_number'];
    $buildingName = $row['building_name'];
    $residentID = $row['id'];
    
    //Deprecated
    //$managerID = $row['manager_id'];
/*
    echo "<br>isadmin: ".$isAdmin."<br>";
    echo "<br>building/room: ".$buildingName." ".$room."<br>";
    echo "<br>residentid: ".$residentID."<br>";
    */
    lookupThermostatID();

  }




  //lookup thermostat_account_id from nest_room_number_X_account
  function lookupThermostatID(){
    global $mysqli,$room,$buildingName,$thermostatAccountID;
    //echo "<br>";
    $table = 'nest_room_number_X_account';
    $query = "SELECT * FROM $table WHERE room_number='$room' AND building_name='$buildingName'";
    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_ASSOC);
    $thermostatAccountID = $row['thermostat_account_id'];

    //echo "<br>thermostatAccountID:  ".$thermostatAccountID."<br>";
    getThermostatAccount();
  }

    //lookup thermostat_account_id from nest_room_number_X_account
  function getThermostatAccount(){
    global $mysqli,$thermostatAccountID, $thermostatUsername, $thermostatPassword;
    //echo "<br>";
    $table = 'nest_thermostat_account';
    $query = "SELECT * FROM $table WHERE id='$thermostatAccountID'";
    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
    $row = mysqli_fetch_array($results, MYSQL_ASSOC);
    $thermostatUsername = $row['username'];
    $thermostatPassword = $row['password'];
    //echo "<br>username/password:  ".$thermostatUsername. " ". $thermostatPassword."<br>";
    getMyResidents();
  }






  //===================ADMIN SPECIFIC===================//

  //TODO: CHANGE TO BUILDING ASSOCIATED WITH MANAGER INSTEAD OF WHERE MANAGER LIVES
  //GET ALL OF THE RESIDENTS UNDER THIS ADMIN/MANAGER
  function getMyResidents(){
    global $mysqli,$isAdmin,$residentID,$buildingName;
    if(!$isAdmin){
      //echo "NOT ADMIN!!!";
      return null;
    }

    $table = 'nest_resident_account';
    $query = "SELECT * FROM $table WHERE building_name='$buildingName'";
    $results =$mysqli->query($query) or die(mysqli_error($mysqli));
   
    while($row = mysqli_fetch_array($results, MYSQL_ASSOC)){
      //echo $row['MyID'].": ".$row['room_number'].'<br>';
      //echo "****<br>";
    }
  }



?>
