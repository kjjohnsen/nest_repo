SQL is standard ANSI

The data in RDBMS is stored in database objects called tables. 
RELATIONAL DATABASE MANAGEMENT SYSTEM

Rules
****************************************************

-Insert single quotes around STRINGS and no quotes around NUMBERS
-use PARENTHESIS with AND / OR

Select
*********************************************************************************
Select * FROM Customers  -> selects all the data from table Customers
Select Name, City FROM Customers -> selects Name + City from Customers
                                                                                

SELECT DISTINCT
*********************************************************************************
returns different data from table
if there are 50 Atlanta's, it will only return one.


AND / OR COMBINATIONS
*********************************************************************************
Select City FROM Customers WHERE Name='John' AND (location='there' OR location='here')


ORDER BY
*********************************************************************************
-default is ASC, but you can do DESC

Insert Into
*********************************************************************************
-Two forms: specify column names or don't 

INSERT INTO Customers (Name, height, weight) VALUES ('John, 72, 178)

not sure if ints should be in braces or not -> NOPE-> the above syntax is correct!

Update
*********************************************************************************
UPDATE charlie_table
SET User='Charlie', MyID='ccurtis9'
WHERE number='810347652'

DELETE
*********************************************************************************
DELETE FROM charlie_table
WHERE User='ccurtis9'

SQL Injections
*********************************************************************************
-Security Issues
 http://www.w3schools.com/sql/sql_injection.asp
-There are ways to avoid this: add @ to the beginning of their query

LIMIT
*********************************************************************************
SELECT * FROM charlie_table LIMIT 10;

LIKE
*********************************************************************************
SELECT * from charlie_table WHERE name LIKE 's%' // picks entries that do start with an s

SELECT * from charlie_table WHERE name NOT LIKE '%s' //picks entries that do not end in S

WILDCARDS
*********************************************************************************

% -> a substitute for zero or more characters

_ -> a substitute for a single character

[a-z]-> ranges of chracters that it looks for

[^a-z]-> matches characters NOT within the brackets
[!a-z]-> matches characters NOT within the brackets

IN
*********************************************************************************

basically prevents you from writing alot of OR statements

select * FROM charlie_table WHERE user='Mike' OR user='Sam' or user='Scott'

SELECT * FROM charlie_table WHERE user IN ('Mike', 'Sam', 'Scott')

BETWEEN
*********************************************************************************

SELECT * FROM charlie_table height BETWEEN 72 and 80

ALIAS
*********************************************************************************
-creates a temporary name for a table so it is more readable


SQL JOIN
*********************************************************************************
Inner Join- (simple join) joins tables together based on common column(s)

//gets the three columns mentioned, and moves them into CUSTOMERS

SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
FROM Orders
INNER JOIN Customers
ON Orders.CustomerID=Customers.CustomerID;


The INNER JOIN keyword selects all rows from both tables as long as there is a match between the columns in both tables.





