https://developer.nest.com/documentation/cloud/nest-api-intro

*See the second bulletpoint w/ firebase libraries; there is example code


https://developer.nest.com/documentation/cloud/api-overview/

The data is in the form of JSON objects

Data that is useful to the user
*******************************************
 software_version
 name
 name_long
 can_cool
 can_heat
is_using_emergency_heat
has_fan
has_leaf
temperature_scale
target_temperature_f
target_temperature_c
target_temperature_high_c
target_temperature_high_f
target_temperature_low_f
target_temperature_low_c
away_temperature_high_c
away_temperature_high_f
away_temperature_low_f
away_temperature_low_c
hvac_mode
ambient_temperature_c
ambient_temperature_f
humidity

Data that the user doesn't need but should log
************************************************
 device_id
 locale
 structure_id
 last_connection
 is_online
smoke_co_alarms
fan_timer_active
fan_timer_timeout


there are more functions available in the URL listed above