
#====== Nest Thermostat Statement of Work ======#

##==== Background ====##

We were provided two Nest Thermostats in order to further extend the capabilities of a Nest Thermostat. The goal of this project is to design 
a platform for collecting and analyzing data from a Nest thermostat. The purpose of this project is to better inform building administrators of 
the energy consumption within the building by providing statistics from the Nest Thermostat. This project extends the functionality of what 
Nest provides by implementing data logging. It is necssary that the platform does not violate the privacy of the user in future implementations, 
and should also abide to the terms of conditions that Nest as already setfourth. Note that this project is being used for research purposes only.

----


##==== Objectives ====##

Build a system for Nest Thermostats that:

  * Can log, analyze, interpret, and visualize data
  
  * Can log data from an indefinite number of Nest's Thermostats
  
  * Can better inform building managers of energy consumption in specific units
  
  * Allow a building manager to monitor thermostat data while giving control to the residents


----

##==== Scope ====##

As a project owner, the group will design all hardware and software that is needed to implement this system and modify the existing Nest Thermostats. This system will be
according to the problem statement that was formulated between the group and its stakeholders: Tom Lawrence, and Kyle Johnsen.


----

##==== System Requirements ====##

  * Should update data from all Nest Thermostats every 1 second(s)
  * Should atleast provide the following data throughout the day: temperature, away mode (whether on or off), average daily temperature, number of leafs, humidity
  * Should be able to operate on 24V power
  * Should have wireless network capabilities (802.11b/g/n @ 2.4 GHz)
  * User interface must run on an Apache webserver
  * Webpage should be responsive for mobile devices
  * Response time should be less than 100ms
  * User interface should be intiutive
  * Should provide monthly energy comparisions no less than once every 30 days.
  * Should be able to provide estimated daily and monthly energy savings 
  * Should provide a day-to-day breakdown of hourly temperature which is available no later than the day after
  * Any hardware modifications should fit within the Nest Thermostat (3.27" diameter x 1.10" height)


----

##==== Work Area Overview ====##

  * **Task 1** Background Research and Preliminary Setup
      * **Task 1.1** Research Nest's capabilities and limitations
      * **Task 1.2** Set up Nest accounts and sync thermostats to respective accounts
  * **Task 2** Database Design and Implementation
      * **Task 2.1** Draw up basic conceptual data model outlining flow of information
      * **Task 2.2** Create database based on conceptual model
      * **Task 2.3** Automate retrieval of data from Nest API for populating database
      * **Task 2.4** Implement messaging system between client and database 
  * **Task 3** User Interface Design and Creation
      * **Task 3.1** Draw up wireframes and storyboards for website
      * **Task 3.2** Create working interface based on wireframes/storyboards
      * **Task 3.3** Connect user interface to database
  * **Task 4** Final prototype
      * **Task 4.1** Design and create poster
      * **Task 4.2** Complete evaluations
      * **Task 4.3** Prepare for final presentation


##==== Task Details ====##


###== Task 1: Background Research and Preliminary Setup ==###

This task involves researching the customer's needs and creating the best way to meet those needs. After understanding the problem, the task is to research the Nest Thermostat and the systems connected with it. Consider the physical and technical capabilities and limitations and discover ways to utilize and overcome them respectively. To access the data from the thermostats, Nest accounts need to be created. Multiple accounts will need to be created since accounts can only connect to ten thermostats. The accounts will then be synced together for the next task.


###== Task 2: Database Design and Implementation ==###

This task will involve designing and implementing a solution using a combination of software and/or hardware to automate the collection of environmental data from the housing units, either through the existing Nest API, or through a custom designed API. The database will provide access to the aggregate data collected, as well as supply data sorting and collection to present the data in a logical and more useful way.


###== Task 3: User Interface Design and Creation ==###

This task will involve designing detailed wireframes and storyboards of the proposed website. Before implementing the design, the team will ask for 
Dr. Johnsen's and Dr. Lawrence's opinions on the design. After receiving feedback from Dr. Johnsen and Dr. Lawrence, the team will implement the user
interface, including all suggested changes. Once the team has a working user interface (not necessarily linked to database), Dr. Lawrence and Dr. Johnsen
will be asked for more suggestions. Finally, once both professors are content with the interface, the team will link the user interface and database (if it
has not already been linked).



###== Task 4: Final prototype ==###

This task will involve integrating all of the above tasks as well as testing the complete system in order to ensure that the final prototype is functional. It will also include the development of any additional mock ups or demonstrations that will be used to demonstrate how the system will work.


----

##==== Delivery ====##

On the day of the presentation (late April), the group will deliver a presentation and poster describing the details of the system as well as a small scale prototype that will demonstrate how it will work.
The prototype will a website used to interact with a network of two Nest Thermostats.



----

##==== Place of Performance ====##

All work will be completed at the University of Georgia/Driftmier Engineering Center


----

##==== Period of Performance ====##


The project was started on January 8, 2015 and will be completed no later than May 6, 2015