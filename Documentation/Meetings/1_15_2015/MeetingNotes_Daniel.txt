Dr. Lawrence meeting 1/15:

idea of project is to see if installing nest in apartment would reduce energy consumption

what impact for low tech users, maybe they move in and it's already installed; how well will it save them energy

Brandon Oaks potential test site

want to collect data on occupied/unoccupied maybe (privacy issue)

collect data through power transmission to the heat pumps, standard residential unit

see what temperature setting is vs actual temperature

how does the nest handle two occupants changing temp to their own liking

Georgia Power wants to know how much savings nest can provide with people not using any special features, simply adjusting 	the dial like an old style thermostat

units in seperate apartment dwellings, want to view info of each unit seperately

develop potential extended interface to provide user with extra information

how does the thermostat handle if current owner moves out and new owner wants access? is the nest locked to one single 		account

should building codes require some sort of smart thermostat

potentially network nest units to a master data logger and collect data in background for review, how to circumvent 10 unit 	cap limit

no current nest development contact

should have first unit by monday next week, probably only one unit for now

first step: collect data from the nests
how? 

grand idea of the project: moving forward into a more interconnected world, how will using smart technology affect energy 	consumption and reduction, even if the users of the smart device do not fully explore its capabilities; how will a 	nest change energy use if people treat it as a standard thermostat and aren't very interested in its features

what is the deliverable? proof or counterproof of benefit of cost of building wide installation? visualiztion of cost 	reduction of HVAC with nest units vs standard units