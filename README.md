#//==================== README ====================//#

***
##//========================= Overview ==========================//##

**CLASS:**  
> CSEE 4920 (Senior Design)

**PROJECT:**     
> [Nest Thermostat](http://sensornetworks.engr.uga.edu/nest/)

**SPONSOR/MENTORS:**
> **Dr. Tom Lawrence** (sponsor)

> **Dr. Kyle Johnsen** (primary mentor)

> **Dr. Peter Kner** (secondary mentor)

**TEAM:** [(Group Timelog)](https://bitbucket.org/elliotttanner1/nest_repo/wiki/Group%20Work%20Breakdown)

> [Chen, James](https://bitbucket.org/elliotttanner1/nest_repo/wiki/Timelog%20James)

> [Curtis, Charlie](https://bitbucket.org/elliotttanner1/nest_repo/wiki/Timelog%20Charlie)

> [Goss, Tyler](https://bitbucket.org/elliotttanner1/nest_repo/wiki/Timelog%20Tyler)

> [Tanner, Elliott](https://bitbucket.org/elliotttanner1/nest_repo/wiki/Timelog%20Elliott)

> [Wade, Daniel](https://bitbucket.org/elliotttanner1/nest_repo/wiki/Timelog%20Daniel)

***

##//========================= Timeline ==========================//##
![Screen Shot 2015-02-03 at 12.01.32 PM.png](https://bitbucket.org/repo/zek9dj/images/3606334411-Screen%20Shot%202015-02-03%20at%2012.01.32%20PM.png)